const User = require('../models/userModel');
const auth = require('../auth');
const Product = require('../models/productModel');
const bcrypt = require('bcrypt');
const _ = require('lodash');

// Validate if email already exist in the database.
exports.checkEmailExists = (registerDetails) => {
	const {email} = registerDetails;
	return User.findOne({email: email}).then(user => {
		if(user) return false
		else return true
	})
};

// Create New User in the db.
exports.createNewUser = (registerDetails) => {
	console.log(registerDetails)
	const numberOfSalts = 10;
	const {firstname, lastname, gender, birthday, email, password, lotNumber, barangay, city, province, country} = registerDetails;
	const firstName = _.capitalize(firstname);
	const lastName = _.capitalize(lastname);
	const encryptedPass = bcrypt.hashSync(password, numberOfSalts);

	const newUser = new User({
		fullName:[{
			givenName: firstName,
			familyName: lastName
		}],
		address:[{
			lotNumber: lotNumber,
			barangay: barangay, 
			city: city, 
			province: province, 
			country: country 
		}],
		gender: gender,
		birthDate: birthday,
		email: email,
		password: encryptedPass
	})
	
	return newUser.save().then((user, err) => {
		if(err) return false 
		else return user
	})
};

// Get all Existing Users 
exports.fetchAllUser = (userData) => {
	return User.find({}).then(user => { 
		if(userData.isAdmin) return user
		else return false; 
	});
}

// Get certain user.
exports.getProfile = (userId) => {
	const {id} = userId
	console.log(id);
	return User.findById(id).then(user => {
		if(user) return user
		else return false
	}).catch(() => {
		return false
	})
}

// Login User
exports.login = (userDetails) => {
	const {email, password} = userDetails
	return User.findOne({email: email}).then(user => {
		if(user){
			const isPasswordMatch = bcrypt.compareSync(password,user.password);
			if(isPasswordMatch) return {access: auth.createAccessToken(user)}
			else return false
		}else return false
	})
}

// Update User
exports.updateUserInfo = (userDetailsChange, userId) => {
	const {id} = userId;
	const { firstname, lastname, lotNumber, barangay, city, province, country, gender, birthDate} = userDetailsChange;
	let updatedUserInfo = {
		fullName:[{
			givenName: firstname,
			familyName: lastname
		}],
		address:[{
			lotNumber: lotNumber,
			barangay: barangay, 
			city: city, 
			province: province, 
			country: country 
		}],
		gender: gender,
		birthDate: birthDate,
	}
	return User.findById(id).then(user => {
		if(user){
			Object.assign(user, updatedUserInfo).save()
			return user
		}else return false
	})
}

// Validate Password 
exports.validatePassword = (userDetails, userId) => {
	const { id } = userId;
	const { password } = userDetails;
	return User.findById(id).then(user => {
		if(user){
			const isPasswordMatch = bcrypt.compareSync(password,user.password);
			if(isPasswordMatch) return user
			else return false
		}else return false
	})
}

// Disable User
exports.disableUser = (userDetailsChane, userId) => {
	const { id } = userId;
	return User.findById(id).then(user => {
		if(user){
			Object.assign(user, userDetailsChane).save()
			return user 
		}else return false
	})
}

// Check if user still active
exports.validateUserActive = (userId) => {
	return User.findById(userId.id).then(user => {
		if(user.isActive) return true
		else return false
	})
}

// Enable and Disable Admin 
exports.assignAdminUser = (userDetailsChange, userId, userData) => {
	const { id } = userId;
	return User.findById(id).then(user => {
		if(user && userData.isAdmin){
			Object.assign(user, userDetailsChange).save()
			return user 
		}else return false
	})	
}

// Forgot Password
exports.forgotPassword = (userDetails) => {
	const numberOfSalts = 10;
	const password = bcrypt.hashSync(userDetails.password, numberOfSalts);
	const newPassword = {
		password: password
	}
	return User.findOne({email: userDetails.email}).then(user => {
		if(user.birthDate == userDetails.birthDate){
			Object.assign(user, newPassword).save()
			return user
		}else return false;
	})
} 

// Change Password
exports.changePassword = (userDetails, userId) => {
	const numberOfSalts = 10;
	const newPassword = bcrypt.hashSync(userDetails.newPassword, numberOfSalts);
	const oldPassword = userDetails.oldPassword;
	const changedPassword = {
		password: newPassword
	}	
	return User.findById(userId.id).then(user => {
		if(user){
			const isPasswordMatch = bcrypt.compareSync(oldPassword,user.password);
			if(isPasswordMatch){
				Object.assign(user, changedPassword).save()
				return user;
			}else return false;
		}else return false;
	})
}

// purchase product 
exports.productBought = async(data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.purchased.push({productId: data.productId, numberProductsBought: data.boughtProducts})
		return user.save().then((user,err) => {
			if(err) return false
			else return user
		})	
	});

	let isProductUpdate = await Product.findById(data.productId).then(product => {
		product.purchaser.push({userId: data.userId})
		return product.save().then((product, err) => {
			if(err) return false
			else return product
		})
	})

	if(isUserUpdated && isProductUpdate) return true
	else return false
}

// Add Favorite Product 
exports.favoriteProduct = async(data, productId) => {
	console.log(data)
	console.log(productId)
	let isUserFavorite = await User.findById(data.userId).then(user => {
		user.wishlisted.push({ productId: productId.id, starred: data.wished })
		return user.save().then((user, err) => {
			if(err) return false
			else return user
		})
	})

	let isProductFavorite = await Product.findById(productId.id).then(product => {
		product.wishlister.push({ userId: data.userId });
		return product.save().then((user, err) => {
			if(err) return false
			else return product 
		})	
	})

	if(isUserFavorite && isProductFavorite) return true
	else return false
} 
