const Product = require('../models/productModel');

// Register Product
exports.registerProduct = (productDetails) => {
	const { name, description, type, price, numberOfStocks, availableStock } = productDetails;
	
	const newProduct = new Product({
		name: name, 
		description: description,
		type: type,
		price: price,
		stocked: numberOfStocks,
		available: availableStock
	})

	return newProduct.save().then((product, err) => {
		if(err) return false
		else return product
	})
}; 

// Show Available Product
exports.showAllProducts = () => {
	return Product.find({}).then(product => {
		return product
	})
};

// Show specific product
exports.showSpecificProduct = (productId) => {
	const {id} = productId;
	return Product.findById(id).then(product => {
		if(product) return product 
		else return false
	})
};

// Archive Product 
exports.archiveProduct = (productDetail ,productId, userData) => {
	const { id } = productId;
	return Product.findById(id).then(product => {
		if(product && userData.isAdmin){
			Object.assign(product, productDetail).save();
			return product; 
		}else return false;
	})	
}

// Update Product
exports.updateProduct = (productDetailChange, productId) => {
	const {id} = productId;
	return Product.findById(id).then(product => {
		if(product){
			Object.assign(product, productDetailChange).save();
			return product;
		}else return false;
	})
}

// Rate Product 
exports.commentProduct = async(data, productId) => {
	let isProductCommented = await Product.findById(productId.id).then(product => {
		product.comment.push(
			{
				userId: data.userId, 
				rate: data.rate,
				title: data.title,
				description: data.description
			}
		)

		return product.save().then((product, err) => {
			if(err) return false
			else return product 
		})
	})

	if(isProductCommented) return true 
	else return false
}
