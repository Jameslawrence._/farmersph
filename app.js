/* https://gentle-dusk-18521.herokuapp.com/ | https://git.heroku.com/gentle-dusk-18521.git */
require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const app = express();
const cors = require('cors');
const userRoute = require('./routes/userRoute');
const productRoute = require('./routes/productRoute');

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors())

const connect = mongoose.connection;
mongoose.connect(process.env.URL, {useNewUrlParser: true, useUnifiedTopology: true});
connect.on('error', console.error.bind(console, 'ERROR: Failed to Connect to the Database'));
connect.once('open', () => { console.log('Successfully Connected to the Database')});

app.use('/user', userRoute);
app.use('/product', productRoute);

app.listen(process.env.PORT || process.env.LOCAL_PORT, () => {
	console.log(`Server is running`);
})
