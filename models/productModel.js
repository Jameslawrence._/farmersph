const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name: String,
	description: String,
	type: String,
	price: Number, 
	stocked: Number,
	available:Number,
	isOutOfStock: {
		type: Boolean,
		default: false
	},
	isActive: {
		type: Boolean, 
		default: true
	},
	createdOn: {
		type: Date, 
		default: new Date()
	},
	purchaser:[
		{
			userId: String,
			purchasedOn:{
				type: Date,
				default: new Date()
			}
		}
	],
	comment:[
		{
			userId: String,
			title: String,
			description: String,  
			rate: Number,
			ratedOn: {
				type: Date,
				default: new Date()
			}
		}
	],
	wishlister: [
		{
			userId: String, 
			wishlistedOn: {
				type: Date,
				default: new Date()
			}
		}
	]

})

module.exports = mongoose.model('Product', productSchema)