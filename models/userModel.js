const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	fullName: [
		{
			givenName:
			{
				type: String,
				required: [true, "Given Name is required"]
			},
			familyName:
			{
				type: String,
				required: [true, "Family name is required"] 
			}
		}
	],
	address: [
		{
			lotNumber: String,
			barangay: String, 
			city: String, 
			province: String, 
			country: String 
		}
	],
	gender: String, 
	birthDate: String,
	email: String,
	googleId: String,
	password: String,
	isActive: {
		type: Boolean,
		default: true
	},
	isAdmin: {
		type: Boolean,
		default: false 
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	purchased: [
		{
			productId: String,
			numberProductsBought: Number,
			purchasedOn:{
				type: Date,
				default: new Date()
			},
			status:{
				type: String,
				default: "pending"
			}
		}
	],
	wishlisted: [
		{
			productId: String, 
			wishlistedOn: {
				type: Date, 
				default: new Date()
			},
			starred:{
				type: Boolean, 
				default: false 
			}
		}
	]	
})

module.exports = mongoose.model('User', userSchema);
