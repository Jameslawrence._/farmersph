const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

router.get('/', auth.verify ,(req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.fetchAllUser(userData).then(resultFromController => res.send(resultFromController))
});

router.get('/:id', (req, res) => {
	userController.getProfile(req.params).then(resultFromController => res.send(resultFromController))
});

router.post('/register', (req, res) => {
	console.log(req.body)
	userController.createNewUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post('/login', (req, res) => {
	userController.login(req.body).then(resultFromController => res.send(resultFromController))
});

router.post('/check-email', (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

router.patch('/update-info/:id', (req, res) => {
	userController.updateUserInfo(req.body, req.params).then(resultFromController => res.send(resultFromController))
});

router.post('/validate-password/:id', (req, res) => {
	userController.validatePassword(req.body, req.params).then(resultFromController => res.send(resultFromController))
});

router.get('/validate-active/:id', (req, res) => {
	userController.validateUserActive(req.params).then(resultFromController => res.send(resultFromController))
});

router.patch('/disable/:id', auth.verify,(req, res) => {
	userController.disableUser(req.body, req.params).then(resultFromController => res.send(resultFromController))
});

router.patch('/assign-admin-status/:id', auth.verify,(req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.assignAdminUser(req.body, req.params, userData).then(resultFromController => res.send(resultFromController))
});

router.post('/purchase', (req, res) => {
	let data = {
		userId: req.body.userId,
		productId: req.body.productId,
		boughtProducts: req.body.numberProductsBought
	}
	userController.productBought(data).then(resultFromController => res.send(resultFromController))
})

router.patch('/forgot-password', (req, res) => {
	userController.forgotPassword(req.body).then(resultFromController => res.send(resultFromController));
})

router.patch('/change-password/:id', auth.verify,(req, res) => {
	userController.changePassword(req.body, req.params).then(resultFromController => res.send(resultFromController));
})
module.exports = router;

