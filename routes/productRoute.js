const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');

router.post('/register', (req, res) => {
	productController.registerProduct(req.body).then(resultFromController => res.send(resultFromController))
})

router.get('/', (req, res) => {
	productController.showAllProducts().then(resultFromController => res.send(resultFromController))
})

router.get('/:id', (req, res) => {
	productController.showSpecificProduct(req.params).then(resultFromController => res.send(resultFromController))
})

router.post('/archive/:id', auth.verify,(req, res) => {
	const userData = auth.decode(req.headers.authorization)
	productController.archiveProduct(req.body, req.params, userData).then(resultFromController => res.send(resultFromController))
})

router.patch('/update/:id', (req, res) => {
	productController.updateProduct(req.body, req.params).then(resultFromController => res.send(resultFromController))
})

router.post('/rate-product/:id', (req, res) => {
	productController.commentProduct(req.body, req.params).then(resultFromController => res.send(resultFromController))
})


module.exports = router